<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id()->comment('primary id');
            $table->bigInteger('code')->comment('code for student');
            $table->char('student_name', 255)->comment('student name');
            $table->char('class', 100)->comment('student class');
            $table->date('birthday')->comment('student birthday');
            $table->string('gender')->comment('student gender');
            $table->string('phone_number')->nullable()->comment('studen phone number');
            $table->string('street')->nullable()->comment('student street');
            $table->string('ward')->nullable()->comment('student ward');
            $table->string('district')->nullable()->comment('student district');
            $table->string('city')->nullable()->comment('student city');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
