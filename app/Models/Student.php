<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $table = 'students';

    protected $fillable = [
        'code',
        'student_name',
        'class',
        'birthday',
        'gender',
        'phone_number',
        'street',
    ];

    /**
     * Get the attendance for the student.
     */
    public function attendances()
    {
        return $this->hasMany(StudentAttendance::class);
    }
}
