<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentAttendance;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class StudentAttendanceController extends Controller
{
    const BAD_REQUEST_STATUS = 400;
    const NOT_FOUND_STATUS = 404;
    const SUCCESS_STATUS = 200;
    const CREATE_SUCCESS_STATUS = 201;
    const INTERNAL_SERVER_ERROR = 500;
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'deviceID' => 'required',
            'hanetCode' => 'required|numeric',
            'dateTime' => 'required|date',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => self::BAD_REQUEST_STATUS,
                'message' => $validator->messages(),
            ], self::BAD_REQUEST_STATUS);
        }


        $student = Student::find($request->hanetCode);

        if (empty($student)) {
            return response()->json([
                'status' => self::NOT_FOUND_STATUS,
                'message' => 'wrong hanetCode',
            ], self::NOT_FOUND_STATUS);
        }

        $attendanceTime = strtotime($request->dateTime);

        $beginOfDay = strtotime("today");
        $endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;

        if ($attendanceTime < $beginOfDay || $attendanceTime > $endOfDay ) {
            return response()->json([
                'status' => self::BAD_REQUEST_STATUS,
                'message' => 'wrong attendance time',
            ], self::BAD_REQUEST_STATUS);
        }

        $beginOfDay = date("Y-m-d H:i:s", $beginOfDay);
        $endOfDay = date("Y-m-d H:i:s", $endOfDay);
        $registAttendance = $student->attendances()->whereBetween('created_at', [$beginOfDay, $endOfDay])->first();
        if (!empty($registAttendance)) {
            return response()->json([
                'status' => self::BAD_REQUEST_STATUS,
                'message' => 'student had attendance',
            ], self::BAD_REQUEST_STATUS);
        }

        $attendance = new StudentAttendance();

        $attendance->attendance_time = $request->dateTime;
        $attendance->device_id = $request->deviceID;
        try {
            $attendance = $student->attendances()->save($attendance);
            return response()->json([
                'status' => self::CREATE_SUCCESS_STATUS,
                'message' => 'student attendance success',
                'data' => $attendance,
            ], self::CREATE_SUCCESS_STATUS);
        } catch (Exception $e) {
            return response()->json([
                'status' => self::INTERNAL_SERVER_ERROR,
                'message' => 'have some erro when attendance' . $e->getMessage(),
            ], self::INTERNAL_SERVER_ERROR);
        }
    }

    public function getStudentAttendance(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from' => 'nullable|date',
            'to' => 'nullable|date|after_or_equal:from',
            'class' => 'nullable|string',
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => self::BAD_REQUEST_STATUS,
                'message' => $validator->messages(),
            ], self::BAD_REQUEST_STATUS);
        }

        $data = Student::join('student_attendance', 'students.id', '=', 'student_attendance.student_id')
                ->where(function ($query) use ($request) {
                    if (!empty($request->from)) {
                        $beginOfDay = date('Y-m-d', strtotime($request->from));
                        $query->where(DB::raw('DATE(student_attendance.attendance_time)'), '>=', $beginOfDay);
                    }

                    if (!empty($request->to)) {
                        $endOfDay = date('Y-m-d', strtotime($request->to));
                        $query->where(DB::raw('DATE(student_attendance.attendance_time)'), '<=', $endOfDay);
                    }

                    if (!empty($request->class)) {
                        $query->where('students.class', '=', $request->class);
                    }
                })
                ->orderBy('student_attendance.attendance_time', 'desc')
                ->get(['students.*', 'student_attendance.*']);

        return response()->json([
            'status' => self::SUCCESS_STATUS,
            'data' => $data,
        ], self::SUCCESS_STATUS);
    }
}
