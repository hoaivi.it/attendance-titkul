<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\StudentAttendanceController;

Route::get('/', function () {
    return 'Welcome to my API';
});

Route::post('student-attendance', [StudentAttendanceController::class, 'store']);
Route::get('student-attendance', [StudentAttendanceController::class, 'getStudentAttendance']);
